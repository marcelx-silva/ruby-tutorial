# frozen_string_literal: true


meu_array = Array.new(10)
meu_outro_array = Array.new
outro_array = [10,"vinte",66,4,true,nil]

meu_array << 10
meu_array[1] = 67
# puts meu_array

# métodos aux

array1 = [1, 2, 3]
array2 = [3, 4, 5]

#& - retorna (dá como resultado) um novo array contendo os elementos em comum nos dois arrays
puts "& | retorna (dá como resultado) um novo array contendo os elementos em comum nos dois arrays"
puts array1 & array2

# +  retorna um novo array produzido pela concatenação (união) dos dois arrays
puts "+ | retorna um novo array produzido pela concatenação (união) dos dois arrays"
puts array1 + array2

# -
puts "- | retorna uma cópia do array1 removendo os elementos que também constam no array2."
puts array1 - array2

# ==  == | irá retornar verdadeiro caso o array1 e o array2 tenham a mesma quantidade de
# elementos e se cada elemento do array1 for igual ao seu correspondente (mesma posição) no array2.
puts " == | igualdade"
puts array1 == array2

# clear |  remove todos os elementos do array
puts "clear |  remove todos os elementos do array"
puts array1.clear

# delete | O método irá retornar o próprio valor removido ou nil caso o elemento passado como parâmetro não seja encontrado no array.
puts "delete | O método irá retornar o próprio valor removido ou nil caso o elemento passado como parâmetro não seja encontrado no array."
puts array2.delete(5)

# delete_at | recebe um índice (número inteiro) como parâmetro e remove o elemento armazenado naquela posição.
# O método irá retornar o valor removido ou nil caso o índice passado como parâmetro esteja fora das dimensões do array.
puts "delete_at | recebe um índice (número inteiro) como parâmetro e remove o elemento armazenado naquela posição."
puts array1.delete_at(1)

# empty? | irá retornar verdadeiro caso o array esteja vazio e falso caso contrário.
puts "empty? | irá retornar verdadeiro caso o array esteja vazio e falso caso contrário."
puts array2.empty?
um_array = Array.new
puts um_array.empty?

# include? | recebe um elemento como parâmetro e retorna verdadeiro caso esse elemento esteja presente
# dentro do array, e falso caso contrário.
puts "include? | retorna true se o elemento estiver incluso caso contrário falso"
puts array2.include?(2)
puts um_array.include?(3)

# size | retorna o número de elementos armazenados no array.
puts "size | retorna o número de elementos armazenados no array"
puts array2.size
puts um_array.size