lista = [1]


lista.push(2)
lista.push(3,4)

lista << "Maria"

lista.insert(0,"Nicarágua")

puts lista
puts "Lista de 1 até 3 posição"
puts lista[1..3]

puts "Deletando"
lista.delete("Maria")
puts lista

puts "lista.length"
puts lista.length
puts "lista.count"
puts lista.count

lista_numerica = [3,5,6,1,4,5]

puts "Lista numerica organizada"
puts lista_numerica.sort

lista_caractere = ["Carlos", "Maria", "Reis", "Kate"]

puts "lista string organizada"
puts lista_caractere.sort