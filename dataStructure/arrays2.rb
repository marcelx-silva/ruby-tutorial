# iterando
meu_array = [10, 20, 30, 40, 50]

puts "meu_array.each do |elemento| "
meu_array.each do |elemento|
  puts elemento
end

puts "meu_array.each elemento {}"
meu_array.each { |elemento|
  puts elemento*2
}

puts "for elemento in meu_array"
for elemento in meu_array
  puts elemento*3
end