person = {
  name:"John",
  lastname:"Doe",
  city:"London"
}

for key,value in person
  puts "#{key} = #{value}"
end

person.each { |key, value|
  puts "#{key} = #{value}"
}

print person.keys
print person.values
puts "\n"

i = 0
while i < person.length
  puts "#{person.keys[i]} = #{person.values[i]}"
  i +=1
end