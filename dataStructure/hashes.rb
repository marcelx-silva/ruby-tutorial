#Hashes
h = {}
puts h.class

# :item -> is a symbol
# symbols are unique and refers to the same object
# perform better than a string as key
h = {:name=>"John"}
puts h

#strings can also be keys
h = {"name" => "Maria"}
puts h

#intenger as well
h = {1=>"Jonas"}
puts h

# and arrays

h = {
  ["name", 1] => "Lucas"
}

puts h

# and finally... hashes

h = {
  {:name=>"Gabriel"}=>"Student"
}

puts h

# we can also create a hash using the NEW keyword

h = Hash.new
puts h

h = Hash.new("Default Value")
puts h.default
puts h

h = Hash["number"=>100, 5=>"4"]
puts h

# new syntax to create a hash
# it converts automatically to a symbol
h = { name: "John", lastname: "Doe"}
puts h

