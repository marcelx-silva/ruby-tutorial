h = Hash.new("Default Value")
h[:name]="John"
h[:lastname]="Doe"
h[:city]="London"


puts h.length
puts h.count
puts h.size
puts h.any?
puts h.has_key? :name
puts h.has_value? "John"

puts h.default

puts h.compact
puts h

puts h.empty?
puts h.invert
#
# puts h.to_s
# puts h.to_a

puts h.min
puts h.max

puts h.fetch(:name)

print h.values
print h.keys

puts "\n"
puts h.value? "Car"

puts h.store(:email, "john.doe@example.com")
puts h

puts h.delete(:email)
puts h