# class Person
#   #a constant must start with UPERCASE letter
#   MESSAGE = "Class Constant"
#
#   #class variable
#   # this class is a specie of global variables for its instances
#   # it is not accessible outside of the class, we need to defined a getter and setter.
#   @@counter = 0
#
#   def initialize
#     @@counter += 1
#   end
#
#   #self refers to the class itself
#   def self.counter
#     @@counter
#   end
#
#   def self.counter=(new_counter)
#     @@counter = new_counter
#   end
# end
#
# puts Person::MESSAGE
#
# p1 = Person.new
# p2 = Person.new
# p3 = Person.new
#
# Person.counter = 5
# puts Person.counter