# #inheritance
# class Person
#   def initialize(name, age)
#     @name = name
#     @age = age
#   end
#
#   def test
#     puts "test statement"
#   end
# end
#
# # < - keyword
# class Employee < Person
#   def displayInfo
#     puts "Name: #{@name}\nAge: #{@age}"
#   end
# end
#
# e = Employee.new("John", 40)
# e.displayInfo
# e.test
#
# #we can find all parent of a class with ancestors() method
# p Employee.ancestors
# p String.ancestors