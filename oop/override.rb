class Animal
  def swim
    puts "Animals can swim"
  end
end

class Tiger < Animal
  def swim
    puts "Tigers can swim"
  end
end

tiger = Tiger.new
tiger.swim