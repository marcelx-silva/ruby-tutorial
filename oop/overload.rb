class Point
  attr_accessor :x,:y
  def initialize(x,y)
    @x = x
    @y = y
  end

  def to_s
    "(#{x},#{y})"
  end

  def +(other)
    Point.new(@x + other.x, @y + other.y)
  end
end

# p = Point.new(2,3)
# puts p.to_s


p1 = Point.new(6,7)
p2 = Point.new(5,6)
p3 = p1 + p2
puts p3.to_s