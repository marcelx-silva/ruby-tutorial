# # class Document
# #   def initialize(title)
# #     @title = title
# #   end
# # end
# #
# # class PDF < Document
# #   def print
# #     puts "Printing PDF\nTitle: #{@title}"
# #   end
# # end
# #
# # class Word < Document
# #   def print
# #     puts "Printing Word\nTitle: #{@title}"
# #   end
# # end
# #
# #
# # wordDocument = Word.new "Ruby Examples"
# # wordDocument.print
# #
# # pdfDocument = PDF.new "Ruby Polymorphism"
# # pdfDocument.print
#
# class WhatsApp
#   def call
#     puts "WhatsApp Call"
#   end
# end
#
# class Skype
#   def call
#     puts "Skype Call"
#   end
# end
#
# apps = [Skype, WhatsApp]
#
# apps.each do |obj|
#   obj.new.call
# end