# # extend is used in class instances
# # it allows we to use INSTANCE METHODS from the module
# module ModuleA
#   def self.method1
#     puts "method1: class method"
#   end
#
#   def method2
#     puts "method2: instance class"
#   end
# end
#
# class Test
#
# end
#
# object = Test.new
# object.extend ModuleA
# # it shows an error
# # undefined method `method1' for #<Test:0x0000025840125b70> (NoMethodError)
# #object.method1
#
# object.method2
