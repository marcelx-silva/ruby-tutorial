#prepend
# it works like the include statement
# but the prepend keyword will put the module
# at the bottom of the ancestor chain
# it will make ruby look for the item that will be included
# before the class itself

module WorkerDebugger
  def run(params)
    puts "Running with params: #{params.inspect}"
    # result = super
    # puts "Completed! Result: #{result}"
  end
end
class Worker
  prepend WorkerDebugger
  def run(params)
    puts "Running on params: #{params.inspect}"
    params.map { |i| i**2}
  end
end

p Worker.ancestors
w = Worker.new
w.run [1,2,4,5,7,8]
