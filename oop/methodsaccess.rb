class A
  #all methods in a class is Public by default
  # can be accessed inside and outside of the class
  def method1
    puts "method1: public method from #{self.class}"
  end

  # can only be accessed inside of the class that it is
  # declared
  #private
  def method2
    puts "method2: is a private method from #{self.class}"
  end

  # can be accessed inside of the class that it is
  # declared and in the subclasses of the class
  # with the keyword 'self'
  protected
  def method3
    puts "method3: is protected method from #{self.class}"
  end

  #another to declare a private method is:
  private :method2
end


class B < A
  def test
    #method3 # error
    self.method3
  end
end
a = A.new
a.method1

# private method `method2' called for #<A:0x000001ac2aa174f8> (NoMethodError)
# a.method2
b = B.new
b.test