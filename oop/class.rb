# #to create a class is used the class reserved word
#
# class Person
#
# end
#
# p1 = Person.new
# p2 = Person.new
#
# #it prints the location in memory of these instantiations
# puts p1
# puts p2
#
# puts p1.class