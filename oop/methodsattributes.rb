# class Person
#
#   # attr_reader is a shortcut for getter method
#   attr_reader :name
#   attr_reader :age
#
#   # attr_writter is a shortcut for setter method
#   attr_writer :name
#   attr_writer :age
#
#   # attr_accessor is a shortcut for both methods
#   attr_accessor :sex
#
#   #a constructor
#   # if the constructor is empty it must contain the
#   # 'super' keyword inside of it
#   def initialize(name,age,sex)
#     #instance variables cannot be accessed
#     # directly outside of its class
#     # it is necessary a getter and setter
#     @name = name
#     @age = age
#     @sex = sex
#   end
#
#   def displayPerson
#     puts "Name: #{@name}\nAge: #{@age}\nSex: #{@sex}"
#   end
#
#   # def name
#   #   @name
#   # end
#   #
#   # def age
#   #   @age
#   # end
#   #
#   #
#   # def name=(new_name)
#   #   @name = new_name
#   # end
#   #
#   # #setter
#   # def age=(new_age)
#   #   @age = new_age
#   # end
# end
#
# p1 = Person.new("Nathalia",26, 'F')
# p1.displayPerson
#
# puts p1.age
# puts p1.name
#
# p1.name = "아"
# puts p1.name
#
# puts p1.sex