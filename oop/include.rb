# module ModuleA
#   def dummy
#     puts "Dummy from moduleA"
#   end
# end
#
# module ModuleB
#   def dummy
#     puts "Dummy from ModuleB"
#   end
# end
#
# # when we use include the Modules will stay
# # between the class and their parents
# # [Worker, ModuleB, ModuleA, Object, Kernel, BasicObject]
# class Worker
#   #it embedded the module into the class
#   # we can use module to organize our code
#   # So we can use Composition rather than Inheritance
#   include ModuleA
#   # if a class has two modules with the same method
#   # it executes the method of the recent one, in other words
#   # it will execute the method from ModuleB because it was
#   # the last included into the class
#   include ModuleB
#   def run
#     dummy
#   end
#
# end
#
# w = Worker.new
# w.run
#
# p Worker.ancestors