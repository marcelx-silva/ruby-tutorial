DIR_PATH = "./filesdirectories/foo/"

#File Operations
# r - read (default)
# w - write
# x - exclusive create
# a - append
# t - text mode (default)
# b - binary mode
# + - updating (read and write)


# fileWriting = File.new("./filesdirectories/foo/file2.txt", "w+" )
#
# puts fileWriting
#
# # writing in a file
# # write() does not add linebreak character
# fileWriting.puts "first line"
# fileWriting.write "second line"
# fileWriting.write "third line"
#
#
# #closing file
# fileWriting.close


# fileReading = File.open(DIR_PATH.concat("file.txt")) do |file|
#   puts file.read
# end
#
# fileReading = File.open(DIR_PATH.concat("file.txt"))
# puts fileReading.read
#
# puts "before rewind()"
# puts fileReading.rewind
# puts fileReading
# puts "after rewind()"
# puts fileReading.readline

# puts File.absolute_path(DIR_PATH.concat("file.txt"))
# puts File.basename(DIR_PATH.concat("file.txt"))

