#Display the current directory path
puts Dir.pwd

#create a directory
#Dir.mkdir "foo"

#delete a diretory
#Dir.rmdir "foo"
# Dir.delete "foo"

# Get all directories and files that match the pattern
#puts "All .rb files"
#puts Dir.glob ".rb"
#puts "All files and dirs from ruby-tutorial directory"
#puts Dir.glob "*"

#Dir.glob "./filesdirectories/*.rb" do |filename|
#  puts filename.gsub(".rb",".js")
#end

# change the current working directory
#puts "ruby-tutorial"
#puts Dir.pwd

# we can also pass an absolute path
#puts "changing the current worki dir..."
#Dir.chdir("./filesdirectories/foo")
#puts Dir.pwd


