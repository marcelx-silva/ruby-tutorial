folder = Dir.new "./filesdirectories/foo"
print folder.entries

puts ""

# we can also pass an absolute path
folder = Dir.open "C:/Users/Thy20/RubymineProjects/ruby-tutorial/filesdirectories/foo"
print folder.entries

puts ""

folder.each do |file|
  puts file
end

# list files from directory that match the pattern inside array
#puts Dir[""]