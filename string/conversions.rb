# Converting a Ruby String to an Array

myArray = "ABCDEFGHIJKLMNO"
puts myArray
puts myArray.kind_of? String
puts myArray.kind_of? Array

myArray = myArray.split
puts myArray

puts myArray.kind_of? String
puts myArray.kind_of? Array

myArray2 = "ABCDEFGHIJKLMNO"
myArray2 = myArray2.split(//)
puts myArray2

myArray = "Paris in the Spring".split(/ /)
puts myArray

myArray = "Red, Green, Blue, Indigo, Violet".split(/, /)
puts myArray

# Changing the Case of a Ruby String
phrase = "paris in the spring"
puts phrase

puts phrase.capitalize # Paris in the spring

puts phrase.upcase # PARIS IN THE SPRING

phrase = "PLEASE DON'T SHOUT!"

puts phrase.downcase # please don't shout!

phrase = "What the Capital And Lower Case Letters Swap"

puts phrase.swapcase #wHAT THE cAPITAL aND lOWER cASE lETTERS sWAP


# Performing String Conversions

stringOrNumber = "100"
puts stringOrNumber
puts stringOrNumber.kind_of? String
puts stringOrNumber.kind_of? Integer

stringOrNumber  = stringOrNumber.to_f
puts stringOrNumber
puts stringOrNumber.kind_of? String
puts stringOrNumber.kind_of? Integer
puts stringOrNumber.kind_of? Float

stringOrNumber = stringOrNumber.to_i
puts stringOrNumber
puts stringOrNumber.kind_of? String
puts stringOrNumber.kind_of? Integer
puts stringOrNumber.kind_of? Float

stringOrNumber = stringOrNumber.to_r
puts stringOrNumber
puts stringOrNumber.kind_of? String
puts stringOrNumber.kind_of? Integer
puts stringOrNumber.kind_of? Float
puts stringOrNumber.kind_of? Symbol
