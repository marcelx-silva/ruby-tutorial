#Concatenating Strings in Ruby

myString = "Welcome" + "to" + "Ruby!"
puts myString

myString = "Welcome" "to" "Ruby!"
puts myString

myString = "Welcome" << "to" << "Ruby!"
puts myString

myString = "Welcome".concat("to").concat("Ruby!")
puts myString

# Freezing a Ruby String
# A string can be frozen after it has been
# created such that it cannot subsequently be altered

myString = "this string"
myString.freeze
##myString << "hello" #error

#Accessing String Elements

myString = "Welcome to Ruby"
puts myString

puts myString["Welcome"]
puts myString["to"]
puts myString["Ruby"]
puts myString[3]
##puts myString["to", "Welcome"] #error
puts myString["TO"] # nothing

# You can also pass through a start position and a number
# of characters to extract a subsection of a string:

puts myString[3, 6]
puts myString[4..10]
puts myString[4...10]

# Comparing Ruby Strings

puts "John" == "Fred"
puts "John" == "John"
puts "John" == "john"

puts "John".eql? "Fred"
puts "John".eql? "John"
puts "John".eql? "john"

"Apples".casecmp "apples"