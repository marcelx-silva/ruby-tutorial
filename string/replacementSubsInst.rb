#Changing a Section of a String

myString = "Welcome to JavaScript!"
puts myString

myString["JavaScript"] = "Ruby"
puts myString

myString = "Welcome to JavaScript!"
myString[8..20]= "Ruby"
puts myString

# Ruby String Substitution
myString = "Welcome to PHP Essentials!"
puts myString

myString = myString.gsub("PHP", "Ruby")
puts myString

myString = "Welcome to PHP"
puts myString
myString.replace "Goodbye to PHP"
puts myString

# Repeating Ruby Strings

myString = "Is that an echo?"
puts myString

puts myString * 3

# Inserting Text into a Ruby String
myString = "Paris in Spring"
puts myString

myString.insert 8, "the"
puts myString

# Ruby chomp and chop Methods
# The purpose of the chop method is to remove the trailing character from a string:

myString = "Paris in the Spring!!"
puts myString

# Note that chop returns a modified string, leaving the original string object unchanged.
# Use chop! to have the change applied to the string object on which the method was called.
puts myString.chop!

# The chomp method removes record separators from a string
myString = "Please keep\n off the\n grass"


myString = myString.chomp
puts myString

# Reversing the Characters in a String
myString = "Paris in the Spring!!"
puts myString.reverse


