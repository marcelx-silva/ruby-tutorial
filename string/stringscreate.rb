myString = String.new
puts myString
puts myString.kind_of? String

myString = String.new("This is my string. Get your own string")
puts myString

myString = String("This is also my string")
puts myString

myString = "Do I need say it again?"
puts myString

#The double quotes are designed to interpret escaped characters
myString = "This is also my string.\nGet your own string"

puts myString

# General Delimited Strings
# Ruby allows you to define any character you want as a string
# delimiter simply by prefixing the desired character with a %.

myString = %&This is my String Again and Again&
puts myString

myString = %&Did you not understand? "MY" string&
puts myString

myString = %(This is my String)
puts myString

myString = %[This is my String]
puts myString

myString = %{This is my String}
puts myString

#Ruby Here Documents
# A Here Document (or heredoc as it is more commonly referred to)
# provides a mechanism for creating free format strings, preserving
# special characters such as new lines and tabs.

myText = <<DOC
Please Detach and return this coupon with your payment.
Do not send cash or coins.

Please write your name and account number on the check and
make checks payable to:

        Acme Corporation

Thank you for your business.
DOC

puts myText