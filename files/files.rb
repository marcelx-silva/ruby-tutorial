# display current path
puts Dir.pwd

# Creating a directory
#Dir.mkdir "./files/foo"
# Dir.mkdir "bar"

# Removing a directory
#Dir.rmdir "bar"

# display file or directories that matches the patter
#puts Dir.glob "*.rb"

Dir.glob("*.rb") do |filename|
  puts filename.gsub("rb","js")
end