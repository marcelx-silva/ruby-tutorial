verdadeiro = true
falso = false

puts verdadeiro && falso
puts verdadeiro || falso
puts falso || falso
puts !verdadeiro || verdadeiro
puts !falso && verdadeiro


puts true and false

puts "IF"

puts "Entre com um valor para X"
x = gets.chomp.to_i

if x == 10
  puts "#{x} é igual a 10"
elsif x > 50
  puts "#{x} é maior que 50"
else
  puts "#{x} não é igual a 10"
end

customerName = "Fred"

customerName == "Fred" ? "Hello Fred" : "Who are you"
puts customerName