#Ruby Sequence Ranges
# inclusive - 1 to 10
puts "inclusive - 1 to 10"
puts (1..10).to_a


# exclusive - 1 to 9
puts "exclusive - 1 to 9"
puts (1...10).to_a

puts "to_a"
puts (1..3).to_a

# ranges are not restricted to numerical values. We can also create a character based range:
puts "ranges are not restricted to numerical values."
puts ('a'..'c').to_a

# Also, we can create ranges based on string values:
# puts "we can create ranges based on string values"
# puts ('cab'..'car').to_a

# Ruby Ranges as Conditional Expressions

# while (input = gets)
#   puts input + "triggered" if input =~ /start/ .. input =~ /end/
# end

# Ruby Range Intervals
puts (1..20) === 15

puts ('k'..'z') === 'm'

