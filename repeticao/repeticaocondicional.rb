# while

puts "Utilizando o While com begin"
begin
  puts "Digite um numero par:"
  numero = gets.chomp.to_i
end while numero % 2 != 0


puts "Utilizando o While"
condicao = true


while condicao
  puts "Digite um numero multiplo de 5:"
  numero = gets.chomp.to_i
  if numero % 5 == 0
    condicao = false
  end
end

puts "#{numero} é multiplo de 5"

# maneira simplificada
# codigo while condicao

#puts "ola" while numero>3

# until
# irá executar repetidamente enquanto a condição for falsa,

puts "Utilizando o Until"
condicao = false

until condicao
  puts "Digite um numero multiplo de 5"
  numero = gets.chomp.to_i
  if numero % 5 == 0
    condicao = true
  end
end
puts "#{numero} é multiplo de 5"

puts "Utilizando o Until com begin"
begin
  puts "Digite um numero multiplo de 7"
  numero = gets.chomp.to_i
end until numero % 7 == 0

puts "o numero digitado foi #{numero}"

# os comandos break, next e redo  funcionam com os laços condicionais.