# estrutura for

for i in 1..10
  puts "O valor de i é #{i}"
end

puts "Digite um número:"
numero = gets.chomp.to_i

for i in 0..10
  puts "#{numero} x #{i} = #{numero*i}"
end

# método each

puts "utilizando o each"
(1..10).each do |i|
  puts "O valor de i é igual a #{i}"
end

# método times
# diferente das outras duas o times inicia o valor da variável em 0

puts "Utilizando o times"
10.times do |a|
  puts "o valor é #{a}"
end

# Controlando a Execução de Laços

puts "Utilizando o break"
# break
for i in 1..10
  if i == 5
    break
  end
  puts "O valor de i é #{i}"
end

puts "Utilizando o next"
# next
for i in 1..10
  if i == 7
    next
  end
  puts "O valor de i é #{i}"
end

# redo - reexecuta a iteração atual

# for i in 1..10
#   puts "O valor de i é #{i}"
#   if i == 3
#     i+1
#     redo
#   end
# end

# retry



# upto - The upto method can be called on Integer, String and Date classes
# and provides similar functionality to the for loop

5.upto(10) do
  puts "HELLO"
end

u = 3

u.upto(7) do
  puts u
end

# downto - The downto method is similar to the upto method with
# the exception that it starts at a value and counts down, rather than up.

15.downto(10) { |i| puts i }

