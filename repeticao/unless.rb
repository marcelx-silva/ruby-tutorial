puts "Com If\n"
puts "Digite o seu nome:"
nome = gets.chomp

if !nome.empty?
  puts "Olá, #{nome}, Bem Vindo!"
end

# é a mesma coisa que

puts "Digite o seu nome:"
nome = gets.chomp

# unless = if not
unless nome.empty?
  puts "Olá, #{nome}, Bem Vindo!"
end

puts "Você digitou o seu nome!" unless nome.empty?
