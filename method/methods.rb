#syntax

def sayHello()
  puts "Hello"
end

sayHello

# passing arguments

def saySomething(something)
  puts something
end

saySomething("Hello Function!")

def multiply(val1, val2)
  puts val1*val2
end

multiply(5,76)

# Passing a Variable Number of Arguments to a Method

def displayStrings(*args)
  args.each { |string| puts string }
end

displayStrings("Nome", "Numero", "Veículo")

# Returning a Value from a Function

def sub(val1, val2)
  val1-val2
end

value = sub(5,7)
puts value


# Ruby Method Aliases

def soma (val1, val2)
  val1 + val2
end

alias doCalc soma

puts doCalc(3,5)

puts doCalc(5,7).even?

def hello(*names)
  names.each { |name| puts name }
end

hello "Jane", "Jack", "Bruce"