students = ["Marcio", "Lucas", "Antonio"]

students.each{ |student| puts student.upcase}

students.each do |student|
  puts student
  puts students.length
end

students.each_with_index do |student, i|
  puts "#{student} - #{i}"
end

# proprieade opcional &param
# the ampersand (&) is going to convert a block to a prop
# prop is a something has method
def my_each(array, &blk)
  unless block_given?
    puts "No Block Given"
  end
  i = 0
  while i < array.count
    el = array[i]
    yield el
    i += 1
  end
end

# blk.call is going to execute this block after "do"
# el == |student|
# el is referencing the student param
my_each(students) do |student|
  puts student
end

def my_each2(array)
  i = 0
  while i < array.count
    yield "TESTANDO"
    i += 1
  end
end

my_each2(students) do |student|
  puts student + "my each2"
end

def do_thrice(&blk)
  blk.call
  blk.call
  blk.call
end

do_thrice do
  puts "Say Hello"
end

def pair((x,y), z)
  puts x
  puts y
  puts z
end
# destructure the array
pair [1,2,7], 5


students = [
  ["Marcio", 67],
  ["Lucas", 98],
  ["Antonio", 78]
]

students.each do |student, grade|
  p student
  p grade
end

# counter = {}
# answers = [1,2,3,3,5,3,4,3,1,1,4,2]
#
# answers.each do |answer|
#   if counter.has_key?(answer)
#     counter[answer] += 1
#   else
#     counter[answer] = 1
#   end
# end
#
# puts counter

counter = Hash.new { |h, k| h[k] = 0}
answers = [1,2,3,3,5,3,4,3,1,1,4,2]

answers.each do |answer|
  counter[answer] += 1
end

puts counter[:a]
puts counter

board = Array.new(2) { Array.new(2) {"empty"}}
puts board