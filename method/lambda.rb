students = ["Marcio", "Lucas", "Antonio"]

def my_each(array, &blk)
  unless block_given?
    puts "No Block Given"
  end
  i = 0
  while i < array.count
    el = array[i]
    blk.call(el)
    i += 1
  end
end


# lambda

print_proc_lambda_1 = -> (student) { puts student}
print_proc_lambda_2 = lambda{ |student| puts student}

puts "print_proc = -> (student) { puts student}"
my_each(students, &print_proc_lambda_1)

puts "print_proc2 = lambda{ |student| puts student}"
my_each(students, &print_proc_lambda_2)

puts lambda {
  |x| x*x
}.call(8)

puts -> (x) { x*x}.call(9)

square = -> (x) { x*x }
puts square.call(43)

#multiline lambda functions

square = lambda do
 |x|
  x * x
end

puts square.call(66)

def testLambda(function, argument)
  function.call(argument)
end

puts testLambda square, 6
