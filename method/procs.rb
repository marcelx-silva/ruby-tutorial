students = ["Marcio", "Lucas", "Antonio"]

def my_each(array, &blk)
  unless block_given?
    puts "No Block Given"
  end
  i = 0
  while i < array.count
    el = array[i]
    blk.call(el)
    i += 1
  end
end

# does not need return keyword in procs
print_proc = Proc.new do |student|
  puts student
end

# proc -> block then proc
my_each(students, &print_proc)

