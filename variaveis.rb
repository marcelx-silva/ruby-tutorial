# frozen_string_literal: true

MinhaConstante = 1
FORMA_CORRETA = "constante"

puts FORMA_CORRETA

# identificando o tipo de variável

minha_var_char = "variavel"
minha_var_int = 56

puts minha_var_char.kind_of? String
puts minha_var_char.kind_of? Integer

puts minha_var_int.kind_of? Integer
puts minha_var_int.kind_of? Float


puts minha_var_int.class
puts minha_var_char.class

# Converting Variable Values

y = 20
puts y

y = y.to_f
puts y

# The to_s() method takes as an argument the number base to which you want the conversion made
# Alternatively, we could convert to binary by specifying a number base of 2:
t = 176
t = t.to_s(2)
puts t

# global variavel
$a = 3
puts $a

# An instance variable
@var = 32
puts @var

# A local variable
local = "local variavel"
_local2 = "local variavel 2"
puts local
puts _local2

# 	A constant
CONSTANTES = 2

#A class variable

#@@clasvariable = 3
#puts @@clasvariable

# Detecting the Scope of a Ruby Variable
# A useful technique to find out the scope of a variable is to use the defined? method.
# defined? will return the scope of the variable referenced, or nil if the variable is
# not defined in the current context:

x = 10
puts defined? x

puts defined? MinhaConstante