# raise - statement to raise an exception

# puts "before exception"
# raise
# #the line below won't be executed
# puts "after exception"

# a=3
# puts "before exception"
# # we can type the Exception
# # we can also raise an exception through a conditional
# raise Exception, "an exception" if a > 4

# #exception handling structure
# begin
#   puts "Processing..."
#   raise "an Exception"
# rescue Exception => e
#   puts "Exception Handled!"
#   puts e.message
# end


# begin
#   puts "Processing..."
#   raise NameError, "custom error message"
# rescue NameError => e
#   puts "Exception Handled!"
#   puts e.message
# end


# begin
#   puts "before exception"
#   a = 3/0
#   puts "after exception"
# rescue Exception => e
#   puts "Exception Handled!"
#   puts "#{e.class}: #{e.message}"
#   # we can see the backtrace
#   puts e.backtrace
# end

# begin
#   puts "before exception"
#   a = 3/0
#   puts "after exception"
# rescue Exception => e
#   puts "Exception Handled!"
#   # we can see the backtrace
#   # another way to see the backtrace and errors is getting them from
#   # the specials global variables
#   puts "#{$!.class}: #{$!.message}"
#   $@.each { |location| puts location }
# end

# begin
#   puts "before exception"
#   puts "test".odd?
#   puts "after exception"
# rescue Exception => e
#   puts "Exception Handled!"
#   puts "#{e.class}: #{e.message}"
#   #we can have multiple rescue statements
# rescue Exception => i
#   puts "#{e.class}: #{e.message}"
# end

# begin
#   puts "before exception"
#   a = 4/7
#   puts "after exception"
# rescue Exception => e
#   puts "Exception Handled!"
#   puts "#{e.class}: #{e.message}"
# rescue Exception => i
#   puts "#{e.class}: #{e.message}"
#   #the else statement if the block executes without problems
# else
#   puts "no errors!"
# end

# begin
#   puts "before exception"
#   a = 4/7
#   puts "after exception"
# rescue Exception => e
#   puts "Exception Handled!"
#   puts "#{e.class}: #{e.message}"
# rescue Exception => i
#   puts "#{e.class}: #{e.message}"
# else
#   puts "no errors!"
#   # ensure statement grants the block inside it executes
#   # independently if an error occurs or not
# ensure
#   puts "ensuring execution of this block"
# end

# fileName = "a.txt"
#
# begin
#   file = File.open(fileName)
#   if file
#     puts "File opened successfully"
#   end
# rescue
#   puts "exception"
#   fileName = "test.txt"
#   # it does the begin block executes again
#   retry
# end


#throw catch
#
# arr = [
#   %w[foo bar wanted test],
#   [1,2,3,4,5],
#   %w[a b c]
# ]
#
# counter = 0
# search = nil
# catch(:found) do
# arr.each do |row|
#   row.each do |item|
#     counter =+ 1
#     # if item == 5
#     #   search = item
#       throw(:found, item) if item == "wanted"
#     end
#   end
#  end
#
# puts "Counter: #{counter}"
# puts "Search: #{search}"